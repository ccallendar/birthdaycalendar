package com.oversoul.birthdays.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Birthday implements Parcelable {

	private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("MMMMM d, yyyy");
	private static final DateTimeFormatter FORMATTER_NO_YEAR = DateTimeFormat.forPattern("MMMMM d");

	private int id;
	private String name;
	private LocalDate birthday;
	private String birthdayString;
	private int gender;
	private String notes;
	private LocalDate dateNotified;
	private int reminderDays;

	public Birthday() {
	}

	public Birthday(String name) {
		this.name = name;
	}

	public Birthday(int id, String name, LocalDate birthday, int gender, String notes, LocalDate dateNotified, int reminderDays) {
		this.id = id;
		this.name = name;
		this.setBirthday(birthday);
		this.gender = gender;
		this.notes = notes;
		this.dateNotified = dateNotified;
		this.reminderDays = reminderDays;
	}

	/**
	 * Creates a new birthday.
	 *
	 * @param name  the person's name
	 * @param year  the year
	 * @param month the month of the year (1-12)
	 * @param day   the day of the month (1-31)
	 */
	public Birthday(String name, int year, int month, int day, int gender, String notes, int reminderDays) {
		this.name = name;
		this.setBirthday(new LocalDate(year, month, day));
		this.gender = gender;
		this.notes = notes;
		this.reminderDays = reminderDays;
	}

	public Birthday(Parcel p) {
		String[] data = new String[5];
		p.readStringArray(data);
		this.id = Integer.parseInt(data[0]);
		this.name = data[1];
		setBirthday(FORMATTER.parseLocalDate(data[2]));
		this.gender = Integer.parseInt(data[3], 10);
		this.notes = data[4];
		String notified = data[5];
		if ((notified != null) && (notified.length() > 0)) {
			this.setDateNotified(FORMATTER.parseLocalDate(notified));
		}
		if ((data.length >= 7) && (data[6] != null) && (data[6].length() > 0)) {
			this.reminderDays = Integer.parseInt(data[6]);
		}
		System.out.println("Birthday from parcel input: " + TextUtils.join(", ", data));
		System.out.println("Birthday from parcel result: " + this.toString());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] {
			String.valueOf(this.id),
			this.name,
			FORMATTER.print(birthday),
			String.valueOf(this.gender),
			this.notes,
			(dateNotified != null ? FORMATTER.print(dateNotified) : ""),
			String.valueOf(reminderDays)
		});
	}

	public static final Parcelable.Creator<Birthday> CREATOR = new Parcelable.Creator<Birthday>() {
		@Override
		public Birthday createFromParcel(Parcel source) {
			return new Birthday(source);
		}

		@Override
		public Birthday[] newArray(int size) {
			return new Birthday[size];
		}
	};

	@Override
	public String toString() {
		return name + " (" + (birthday != null ? FORMATTER.print(birthday) : "null") + "), " + getGenderString();
	}

	public boolean isEqual(Birthday bday) {
		boolean sameName = false;
		boolean sameBirthday = false;
		if (bday != null) {
			String bdayName = bday.getName();
			if (bdayName != null) {
				sameName = bdayName.equalsIgnoreCase(getName());
			}
			LocalDate bdayDate = bday.getBirthday();
			if (bdayDate != null) {
				sameBirthday = bdayDate.equals(getBirthday());
			}
		}
		return sameName && sameBirthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		if (name != null) {
			return name.split(" ")[0];
		}
		return "";
	}

	public String getLastName() {
		if (name != null) {
			String[] split = name.split(" ");
			return (split.length > 1 ? split[split.length - 1] : "");
		}
		return "";
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
		this.birthdayString = (birthday != null ? FORMATTER.print(birthday) : "-");
	}

	public LocalDate getBirthdayThisYear() {
		if (this.birthday != null) {
			LocalDate today = new LocalDate();
			return new LocalDate(today.getYear(), birthday.getMonthOfYear(), birthday.getDayOfMonth());
		}
		return null;
	}

	public LocalDate getBirthdayThisMonth() {
		LocalDate date = null;
		if (this.birthday != null) {
			LocalDate today = new LocalDate();
			try {
				date = new LocalDate(today.getYear(), today.getMonthOfYear(), birthday.getDayOfMonth());
			} catch (IllegalFieldValueException ex) {
				// For days that don't exist in this month - no date?
				date = new LocalDate(today.getYear(), today.getMonthOfYear(), 1).dayOfMonth().withMaximumValue();
				System.out.println("** Day " + birthday.getDayOfMonth() + " doesn't exist in month " + today.getMonthOfYear() +
						", defaulting to " + date.getDayOfMonth());
			}
		}
		return date;
	}

	public LocalDate getNextBirthday() {
		boolean isUnder2 = (getAgeStyle().getYearsOld() < 2);
		return (isUnder2 ? getBirthdayThisMonth() : getBirthdayThisYear());
	}

	public String getBirthdayStringThisYear() {
		return (birthday != null ? FORMATTER_NO_YEAR.print(this.birthday) : "-");
	}

	public String getBirthdayString() {
		return birthdayString;
	}

	public int getYear() {
		return (birthday != null ? birthday.getYear() : 1970);
	}

	/**
	 * Returns the month of the year (1-12)
	 * @return the month of the year (1-12)
	 */
	public int getMonth() {
		return (birthday != null ? birthday.getMonthOfYear() : 1);
	}

	/**
	 * Returns the day of the month (1-31)
	 * @return the day of the month (1-31)
	 */
	public int getDay() {
		return (birthday != null ? birthday.getDayOfMonth() : 1);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getGenderString() {
		return (isBoy() ? "boy" : (isGirl() ? "girl" : "unspecified"));
	}

	public boolean isBoy() {
		return Birthday.isBoy(gender);
	}

	public boolean isGirl() {
		return Birthday.isGirl(gender);
	}

	public static boolean isBoy(int gender) {
		return (gender > 0);
	}

	public static boolean isGirl(int gender) {
		return (gender == 0);
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public LocalDate getDateNotified() {
		return dateNotified;
	}

	public void setDateNotified(LocalDate dateNotified) {
		this.dateNotified = dateNotified;
	}

	public boolean isNotifiedToday() {
		return (this.dateNotified != null) && (new LocalDate()).equals(this.dateNotified);
	}

	public int getReminderDays() {
		return reminderDays;
	}

	public void setReminderDays(int reminderDays) {
		this.reminderDays = reminderDays;
	}

	public boolean isReminderOn() {
		return (this.reminderDays > 0);
	}

	public BirthdayAgeStyle getAgeStyle() {
		return new BirthdayAgeStyle(birthday);
	}

	public String getAgeString(boolean includeName, boolean firstNameOnly, boolean onBirthday) {
		String str = "";
		BirthdayAgeStyle style;
		if (onBirthday) {
			style = new BirthdayAgeStyle(birthday, getNextBirthday());
		} else {
			style = getAgeStyle();
		}
		if (includeName) {
			String nm = (firstNameOnly ? getFirstName() : getName());
			String verb = (onBirthday ? " will be " : " is ");
			String age = style.getAge();
			if (style.isDueDate()) {
				if (onBirthday) {
					age = "born";
				}
				str = nm + verb + age.toLowerCase();
			} else {
				str = nm + verb + style.getAge() + " old";
			}
		} else {
			str = style.getAge();
		}
		return str;
	}

	/**
	 * Returns the number of days away from this person's birthday.
	 * If the baby is under 2 years old, then it checks for days from month-birthday
	 * @return
	 */
	public int getDaysAway() {
		int days = -1;
		if (this.birthday != null) {
			LocalDate now = new LocalDate();
			LocalDate birthdate = getNextBirthday();
			if ((birthdate != null) && birthdate.isAfter(now)) {
				Period p = new Period(now, birthdate, PeriodType.days());
				days = p.getDays();
			}
		}
		return days;
	}

	/**
	 * Checks if the birthday (month + day) matches today, or if the
	 * birthday (day) matches today for dates within the last 2 years.
	 * @return true if the birthday is today
	 */
	public boolean isYearOrMonthBirthdayToday() {
		if (this.birthday != null) {
			return Birthday.isBirthday(this.birthday);
		}
		return false;
	}

	public static boolean isBirthday(LocalDate birthday) {
		LocalDate now = new LocalDate();
		Period p = new Period(birthday, now, PeriodType.yearMonthDay());
		int years = p.getYears();
		int months = p.getMonths();
		int days = p.getDays();
		if ((years < 2) && (days == 0)) {
			// Baby month-iversary
			return true;
		} else if ((months == 0) && (days == 0)) {
			// Birthday
			return true;
		}
		return false;
	}

}

package com.oversoul.birthdays.model;

import android.graphics.Color;
import android.graphics.Typeface;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;


public class BirthdayAgeStyle {

	public static final int COLOR_EXPECTING = Color.rgb(0xE6, 0XC6, 0);
	public static final int COLOR_DUE_DATE = Color.rgb(0xFF, 0x80, 0x2C);
	public static final int COLOR_MONTH_BIRTHDAY = Color.rgb(0x4E, 0xDC, 0);
	public static final int COLOR_YEAR_BIRTHDAY = Color.rgb(0x3E, 0xAE, 0);


	private LocalDate birthday;
	private int color;
	private int font;
	private String age;
	private int yearsOld;
	private boolean dueDate;
	private boolean yearBirthday;
	private boolean monthBirthday;

	/**
	 * Calculates the birthday age styles based on today's date.
	 * @param birthday
	 */
	public BirthdayAgeStyle(LocalDate birthday) {
		this(birthday, null);
	}

	/**
	 * Calculates the birthday styles based on the given date
	 * @param birthday
	 * @param date the date to use in calculations
	 */
	public BirthdayAgeStyle(LocalDate birthday, LocalDate date) {
		this.birthday = birthday;
		this.color = Color.rgb(0x44, 0x44, 0x44);
		this.font = Typeface.NORMAL;
		this.yearBirthday = false;
		this.monthBirthday = false;
		this.yearsOld = 0;
		this.age = calculateAgeString(date);
	}

	public String getAge() {
		return age;
	}

	public int getYearsOld() {
		return yearsOld;
	}

	public int getColor() {
		return color;
	}

	public int getFontStyle() {
		return font;
	}

	public boolean isDueDate() {
		return dueDate;
	}

	public boolean isMonthBirthday() { return monthBirthday; }

	public boolean isYearBirthday() { return yearBirthday; }

	private String calculateAgeString(LocalDate today) {
		if (birthday != null) {
			if (today == null) {
				today = new LocalDate();
			}
			String age;
			if (birthday.equals(today)) {
				font = Typeface.BOLD_ITALIC;
				color = COLOR_DUE_DATE;
				if (today.equals(new LocalDate())) {
					age = "Born today!";
				} else {
					age = "Due date";
				}
				dueDate = true;
			}
			// Special case for future due date, show how many weeks along and weeks to go
			else if (birthday.isAfter(today)) {
				dueDate = true;
				if (font == Typeface.NORMAL) {
					font = Typeface.BOLD;
				}
				color = COLOR_EXPECTING;

				LocalDate conception = birthday.minusWeeks(40);
				String weeksAlong = getWeeksDaysString(conception, today, false);
				String weeksToGo = getWeeksDaysString(today, birthday, false);
				age = weeksAlong + " (" + weeksToGo + " to go)";
			} else {
				age = calculateAgeString(birthday, today);
			}

			return age;
		}
		return "-";
	}

	private String calculateAgeString(LocalDate birthday, LocalDate today) {
		Period p = new Period(birthday, today, PeriodType.yearMonthDay());
		int years = p.getYears();
		int months = p.getMonths();
		int days = p.getDays();
		this.yearsOld = years;

		String age = "";
		//System.out.printf("%d years, %d months, %d days %n", years, months, days);
		if ((years == 0) && (months <= 2) && (days != 0)) {
			age = getWeeksDaysString(birthday, today, true);
		} else {
			age = getYearsMonthsDaysString(years, months, days);
			if ((years < 2) && (days == 0)) {
				// Baby monthiversary
				color = COLOR_MONTH_BIRTHDAY;
				font = Typeface.BOLD;
				if ((years == 1) && (months == 0)) {
					yearBirthday = true;
				} else {
					monthBirthday = true;
				}
			} else if ((months == 0) && (days == 0)) {
				// Birthday!
				color = COLOR_YEAR_BIRTHDAY;
				font = Typeface.BOLD_ITALIC;
				yearBirthday = true;
			}
		}

		return age;
	}

	private static String getYearsMonthsDaysString(int years, int months, int days) {
		if (years < 2) {
			months = (years * 12) + months;
			years = 0;
			// special case for 1st birthday
			if ((months == 12) && (days == 0)) {
				years = 1;
				months = 0;
			}
		}
		return trimComma(plural(years, "year", ", ") + plural(months, "month", ", ") + plural(days, "day", false));
	}

	private static String getWeeksDaysString(LocalDate birthday, LocalDate now, boolean includeDays) {
		Period p = new Period(birthday, now, PeriodType.yearWeekDay());
		int weeks = p.getWeeks();
		int days = p.getDays();
		if (weeks == 0) {
			includeDays = true;
		}
		return trimComma(plural(weeks, "week", ", ") + (includeDays ? plural(days, "day", false) : ""));
	}

	private static String trimComma(String age) {
		age = age.trim();
		if (age.endsWith(",")) {
			age = age.substring(0, age.length() - 1);
		}
		return age;
	}

	private static String plural(int number, String singular, boolean alwaysAdd) {
		return plural(number, singular, null, alwaysAdd);
	}

	private static String plural(int number, String singular, String separator) {
		return plural(number, singular, separator, false);
	}

	private static String plural(int number, String singular, String separator, boolean alwaysAdd) {
		if ((number > 0) || alwaysAdd) {
			return number + " " + singular + (number == 1 ? "" : "s") + (separator != null ? separator : "");
		}
		return "";
	}

	public static String plural(int number, String singular) {
		return plural(number, singular, null, false);
	}

}

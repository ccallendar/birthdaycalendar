package com.oversoul.birthdays;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.oversoul.birthdays.db.BirthdayDbHelper;
import com.oversoul.birthdays.model.Birthday;
import com.oversoul.birthdays.service.BirthdayService;
import com.oversoul.birthdays.settings.SettingsActivity;
import com.oversoul.birthdays.util.FileChooser;

import org.joda.time.LocalDate;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;

public class BirthdaysListActivity extends AppCompatActivity implements View.OnClickListener,
		AdapterView.OnItemClickListener, FileChooser.FileSelectedListener, ActivityCompat.OnRequestPermissionsResultCallback {

	private static final int SORT_BY_FIRST_NAME = 1;
	private static final int SORT_BY_LAST_NAME = 2;
	private static final int SORT_BY_DATE = 3;
	private static final int SORT_BY_UPCOMING = 4;
	private static final int REQUEST_WRITE_PERMISSION = 1;
	private static final int REQUEST_READ_PERMISSION = 2;

	private ListView listView;
	private BirthdayArrayAdapter adapter;
	private ArrayList<Birthday> list = new ArrayList<Birthday>();

	private BirthdayDbHelper dbHelper;
	private BirthdayService birthdayService;

	private int sortBy = SORT_BY_FIRST_NAME;
	private boolean sortInvert = false;
	private boolean noBirthdays = true;
	private final Birthday NO_BIRTHDAY = new Birthday("-- No birthdays --");

	public BirthdayService getBirthdayService() {
		return birthdayService;
	}

	public void setBirthdayService(BirthdayService birthdayService) {
		this.birthdayService = birthdayService;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("BirthdayListActivity", "BirthdaysListActivity created");
		setContentView(R.layout.activity_birthdays_list);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.addItemBtn);
		fab.setOnClickListener(this);

		dbHelper = new BirthdayDbHelper(this);
		list = dbHelper.getBirthdays();
		if (list.size() == 0) {
			System.out.println("There are no birthdays, adding placeholder");
			list.add(NO_BIRTHDAY);
		} else {
			noBirthdays = false;
		}

		listView = (ListView) findViewById(R.id.listView);
		adapter = new BirthdayArrayAdapter(this, list);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);

		if (BirthdayService.getInstance() == null) {
			Log.i("BirthdayListActivity", "Creating BirthdayService from BirthdayListActivity");
			//final ComponentName birthdayServiceComponent = new ComponentName(this, BirthdayService.class);
			Intent myServiceIntent = new Intent(this, BirthdayService.class);
			myServiceIntent.putExtra("messenger", new Messenger(new Handler() {
				@Override
				public void handleMessage(Message msg) {
					BirthdayService service = (BirthdayService) msg.obj;
					setBirthdayService(service);
					if (service.isNotificationsOn()) {
						service.checkBirthdays();
					}
				}
			}));
			startService(myServiceIntent);
		} else {
			Log.i("BirthdayListActivity", "BirthdayService already initialized");
			BirthdayService service = BirthdayService.getInstance();
			setBirthdayService(service);
			if (service.isNotificationsOn()) {
				service.checkBirthdays();
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i("BirthdayListActivity", "START");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.i("BirthdayListActivity", "STOP");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i("BirthdayListActivity", "DESTROY");
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_birthdays_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
			case R.id.action_sort_by_first_name:
				sortBirthdays(SORT_BY_FIRST_NAME);
				return true;
			case R.id.action_sort_by_last_name:
				sortBirthdays(SORT_BY_LAST_NAME);
				return true;
			case R.id.action_sort_by_date:
				sortBirthdays(SORT_BY_DATE);
				return true;
			case R.id.action_sort_by_upcoming:
				sortBirthdays(SORT_BY_UPCOMING);
				return true;
			case R.id.action_settings:
				showSettings();
				return true;
			case R.id.action_export:
				exportDatabase();
				return true;
			case R.id.action_import:
				importDatabase();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private void sortBirthdays(final int sortValue) {
		if (sortBy == sortValue) {
			sortInvert = !sortInvert;
		} else {
			sortInvert = false;
		}
		this.sortBy = sortValue;
		doSortBirthdays();
	}

	private void doSortBirthdays() {
		final LocalDate today = new LocalDate();
		final LocalDate startOfYear = new LocalDate(today.getYear(), 1, 1);
		
		adapter.sort(new Comparator<Birthday>() {
			@Override
			public int compare(Birthday o1, Birthday o2) {
				String n1 = (sortInvert ? o2.getName() : o1.getName());
				String n2 = (sortInvert ? o1.getName() : o2.getName());
				if (sortBy == SORT_BY_LAST_NAME) {
					n1 = reverseName(n1);
					n2 = reverseName(n2);
				}
				LocalDate b1 = (sortInvert ? o2.getBirthday() : o1.getBirthday());
				LocalDate b2 = (sortInvert ? o1.getBirthday() : o2.getBirthday());
				switch (sortBy) {
					case SORT_BY_FIRST_NAME:
					case SORT_BY_LAST_NAME:
						return n1.compareTo(n2);
					case SORT_BY_DATE:
						return compareBirthdates(b1, b2, startOfYear);
					case SORT_BY_UPCOMING:
						return compareBirthdates(b1, b2, today);
					default:
						return 0;
				}
			}

			private String reverseName(String s) {
				String[] split = s.split(" ");
				if (split.length > 0) {
					return split[split.length - 1] + " " + split[0];
				}
				return "";
			}

			private int compareBirthdates(LocalDate b1, LocalDate b2, LocalDate start) {
				int i1 = dateValue(b1, start);
				int i2 = dateValue(b2, start);
				return (i1 - i2);
			}

			private int dateValue(LocalDate d, LocalDate start) {
				int i = d.getDayOfYear() - start.getDayOfYear();
				return (i < 0 ? i + 366 : i);
			}
		});
	}

	private void showSettings() {
		Intent i = new Intent(this, SettingsActivity.class);
		startActivity(i);
	}

	private void exportDatabase() {
		Log.i("BirthdayListActivity", "Exporting database...");
		if (isPermissionGranted(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, REQUEST_WRITE_PERMISSION)) {
			dbHelper.exportDb(getBaseContext(), true);
		}
	}

	private boolean isPermissionGranted(Context context, String permission, int requestCode) {
		boolean granted = true;
		if (Build.VERSION.SDK_INT >= 23) {
			if (context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
				Log.i("BirthdayListActivity", permission + " is granted");
				granted = true;
			} else {
				Log.i("BirthdayListActivity", permission + " is revoked");
				ActivityCompat.requestPermissions(this, new String[]{ permission }, requestCode);
				granted = false;
			}
		} else {
			// Permission is automatically granted on sdk<23 upon installation
			Log.i("BirthdayListActivity", permission + " is already granted");
		}
		return granted;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		Log.i("BirthdayListActivity", "Request: " + requestCode + ", permission: " + permissions[0] + " was " + grantResults[0]);
		if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
			if (requestCode == REQUEST_WRITE_PERMISSION) {
				dbHelper.exportDb(getBaseContext(), true);
			} else if (requestCode == REQUEST_READ_PERMISSION) {
				new FileChooser(this, ".db").setFileListener(this).show();
			}
		}
	}

	private void importDatabase() {
		Log.i("BirthdayListActivity", "Importing database...");
		if (isPermissionGranted(this, android.Manifest.permission.READ_EXTERNAL_STORAGE, REQUEST_READ_PERMISSION)) {
			new FileChooser(this, ".db").setFileListener(this).show();
		}
	}

	@Override
	public void fileSelected(final File file) {
		final Context context = getBaseContext();
		if (dbHelper.importDb(file, context)) {
			list.clear();
			list.addAll(dbHelper.getBirthdays());
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onClick(View view) {
		Log.i("BirthdayListActivity", "Clicked to add item");

		Intent intent = new Intent(this, AddBirthdayActivity.class);
		startActivityForResult(intent, AddBirthdayActivity.RESULT_SAVE);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Birthday bday = list.get(position);
		if (bday == NO_BIRTHDAY) {
			return;
		}
        Intent intent = new Intent(this, AddBirthdayActivity.class);
		intent.putExtra("Id", bday.getId());
		intent.putExtra("Name", bday.getName());
		intent.putExtra("Year", bday.getYear());
		intent.putExtra("Month", bday.getMonth());
		intent.putExtra("Day", bday.getDay());
		intent.putExtra("Gender", bday.getGender());
		intent.putExtra("Notes", bday.getNotes());
		intent.putExtra("Reminder", bday.getReminderDays());
		startActivityForResult(intent, AddBirthdayActivity.RESULT_SAVE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Log.i("BirthdayListActivity", "Request: " + requestCode + ", result: " + resultCode);
		if (resultCode == AddBirthdayActivity.RESULT_SAVE) {
			saveBirthday(data);
		} else if (resultCode == AddBirthdayActivity.RESULT_DELETE) {
			int id = data.getIntExtra("Id", -1);
			Birthday bday = getBirthdayById(id);
			if (bday != null) {
				Log.i("BirthdaysListActivity", "Deleting birthday: " + id);
				promptDeleteBirthday(bday);
			} else {
				Log.w("BirthdaysListActivity", "Birthday to delete not found: " + id);
			}
		}
	}

	private void saveBirthday(Intent data) {
		int id = data.getIntExtra("Id", -1);
		String name = data.getStringExtra("Name");
		int year = data.getIntExtra("Year", 0);
		int month = data.getIntExtra("Month", 1);
		int day = data.getIntExtra("Day", 1);
		int gender = data.getIntExtra("Gender", 1);
		String notes = data.getStringExtra("Notes");
		int reminderDays = data.getIntExtra("Reminder", 0);
		saveBirthday(id, name, year, month, day, gender, notes, reminderDays);
	}

	private void saveBirthday(int id, String name, int year, int month, int day, int gender, String notes, int reminderDays) {
		Log.i("BirthdayListActivity", "Saving birthday, name: " + name + ", date: " + year + "-" + month + "-" + day);

		boolean ok = false;
		if ((name == null) || (name.trim().length() == 0)) {
			name = "[No name]";
		}
		if (year > 0) {
			if (id != -1) {
				Birthday bday = getBirthdayById(id);
				Log.i("BirthdayListActivity", "Updating existing birthday with id " + id + ", bday + " + (bday != null));
				if (bday != null) {
					bday.setName(name.trim());
					LocalDate date = new LocalDate(year, month, day);
					// Clear the notification date
					if (!date.equals(bday.getBirthday())) {
						bday.setDateNotified(null);
						dbHelper.setDateNotified(bday);
					}
					bday.setBirthday(date);
					bday.setGender(gender);
					bday.setNotes(notes);
					bday.setReminderDays(reminderDays);
					ok = dbHelper.updateBirthday(bday);
					Log.i("BirthdaysListActivity", "Update ok " + ok);
				} else {
					Log.e("BirthdaysListActivity", "Unable to find birthday to save, id: " + id);
				}
			} else {
				Log.i("BirthdaysListActivity", "Inserting new birthday");
				Birthday bday = new Birthday(name.trim(), year, month, day, gender, notes, reminderDays);
				ok = dbHelper.insertBirthday(bday);
				Log.i("BirthdaysListActivity", "Insert ok: " + ok);
				if (ok) {
					if (noBirthdays) {
						list.remove(NO_BIRTHDAY);
						noBirthdays = false;
					}
					list.add(bday);
				}
			}
		}
		if (ok) {
			adapter.notifyDataSetChanged();
			doSortBirthdays();
			if (birthdayService.isNotificationsOn()) {
				birthdayService.setupNotifications();
				birthdayService.scheduleBirthdayCheckNow();
			}
		}
	}

	private void promptDeleteBirthday(final Birthday bday) {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which){
					case DialogInterface.BUTTON_POSITIVE:   // Yes button clicked
						deleteBirthday(bday);
						break;
					case DialogInterface.BUTTON_NEGATIVE:   // No button clicked
						break;
				}
			}
		};
		new AlertDialog.Builder(this).setMessage("Are you sure you want to delete this birthday?")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();
	}

	private void deleteBirthday(Birthday bday) {
		if (dbHelper.deleteBirthday(bday.getId())) {
			list.remove(bday);
			if (list.size() == 0) {
				list.add(NO_BIRTHDAY);
				noBirthdays = true;
			}
			adapter.notifyDataSetChanged();
		}
	}

	private Birthday getBirthdayById(int id) {
		for (Birthday bday : list) {
			if (bday.getId() == id) {
				return bday;
			}
		}
		System.err.println("Couldn't find birthday with id " + id);
		return null;
	}

}

package com.oversoul.birthdays;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.oversoul.birthdays.model.Birthday;
import com.oversoul.birthdays.model.BirthdayAgeStyle;
import com.oversoul.birthdays.reminder.ReminderDialog;
import com.oversoul.birthdays.reminder.ReminderListener;

import org.joda.time.LocalDate;

public class AddBirthdayActivity extends AppCompatActivity implements ViewStub.OnClickListener,
		DatePicker.OnDateChangedListener, View.OnKeyListener {

	public static final int RESULT_SAVE = 1;
	public static final int RESULT_DELETE = 2;

	private int editId = -1;
	private int reminderDays = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_birthday);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		findViewById(R.id.saveBirthdayButton).setOnClickListener(this);
		findViewById(R.id.addBirthdayLayout).setOnClickListener(this);
		findViewById(R.id.nameEditText).setOnKeyListener(this);
		getReminderCheckBox().setOnClickListener(this);

		Intent input = getIntent();
		String name = input.getStringExtra("Name");
		if ((name != null) && (name.length() > 0)) {
			int year = input.getIntExtra("Year", 2000);
			int month = input.getIntExtra("Month", 1) - 1;
			int day = input.getIntExtra("Day", 1);
			int gender = input.getIntExtra("Gender", 1);
			String notes = getIntent().getStringExtra("Notes");
			reminderDays = input.getIntExtra("Reminder", 0);
			editId = input.getIntExtra("Id", -1);
			Log.i("AddBirthdayActivity", "Editing: " + year + "-" + month + "-" + day + ", id " + editId);

			getNameEditText().setText(name, TextView.BufferType.EDITABLE);
			getDatePicker().init(year, month, day, this);
			if (Birthday.isBoy(gender)) {
				getBoyRadioButton().setChecked(true);
				getGirlRadioButton().setChecked(false);
				getUnspecifiedRadioButton().setChecked(false);
			} else if (Birthday.isGirl(gender)) {
				getBoyRadioButton().setChecked(false);
				getGirlRadioButton().setChecked(true);
				getUnspecifiedRadioButton().setChecked(false);
			} else {
				getBoyRadioButton().setChecked(false);
				getGirlRadioButton().setChecked(false);
				getUnspecifiedRadioButton().setChecked(true);
			}
			getNotesEditText().setText((notes != null ? notes : ""), TextView.BufferType.EDITABLE);
		} else {
			editId = -1;
			reminderDays = 0;
			LocalDate today = new LocalDate();
			getDatePicker().init(today.getYear(), today.getMonthOfYear()-1, today.getDayOfMonth(), this);
			Log.i("AddBirthdayActivity", "New birthday");

			// Focus name field, show keyboard
			//getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
			getNameEditText().requestFocus();
			//InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			//manager.showSoftInput(getNameEditText(), InputMethodManager.SHOW_IMPLICIT);
		}
		updateReminderCheckBox();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_add_birthday, menu);
		if (editId > 0) {
			menu.findItem(R.id.action_delete).setVisible(true);
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.saveBirthdayButton) {
			saveBirthday();
		} else if (v.getId() == R.id.reminderCheckBox) {
			// Keep it checked (or unchecked)
			getReminderCheckBox().setChecked(reminderDays > 0);
			int days = (editId == -1 ? 1 : reminderDays);
			ReminderDialog d = new ReminderDialog(this, days, new ReminderListener() {
				@Override
				public void reminderSet(int days) {
					reminderDays = days;
					updateReminderCheckBox();
				}
			});
			d.show();
		} else {
			hideKeyboard(v);
		}
	}

	private void updateReminderCheckBox() {
		System.out.println("Reminder: " + reminderDays);
		CheckBox reminderCheckBox = getReminderCheckBox();
		reminderCheckBox.setChecked(reminderDays > 0);
		if (reminderDays == 0) {
			reminderCheckBox.setText(getResources().getString(R.string.reminder_choose));
		} else {
			reminderCheckBox.setText(BirthdayAgeStyle.plural(reminderDays, "day"));
		}
	}

	private void saveBirthday() {
		String name = getNameEditText().getText().toString().trim();
		DatePicker datePicker = getDatePicker();
		int year = datePicker.getYear();
		int month = datePicker.getMonth() + 1;
		int day = datePicker.getDayOfMonth();
		int gender = (getBoyRadioButton().isChecked() ? 1 : (getGirlRadioButton().isChecked() ? 0 : -1));
		String notes = getNotesEditText().getText().toString().trim();
		if ("Notes".equals(notes)) {
			notes = "";
		}
		Log.i("AddBirthdayActivity", "Saving new birthday for " + name + ": " + year + "-" + month + "-" + day +
				", gender: " + gender + (notes.length() > 0 ? ", notes: " + notes : "") + ", edit id " + editId);

		if ((name != null) && (name.length() > 0)) {
			Intent intent = new Intent();
			intent.putExtra("Name", name);
			intent.putExtra("Year", year);
			intent.putExtra("Month", month);
			intent.putExtra("Day", day);
			intent.putExtra("Gender", gender);
			intent.putExtra("Notes", notes);
			intent.putExtra("Reminder", reminderDays);
			intent.putExtra("Id", editId);
			setResult(RESULT_SAVE, intent);
		} else {
			setResult(RESULT_CANCELED);
		}
		finish();
	}

	@Override
	public void onBackPressed() {
		Log.i("AddBirthdayActivity", "onBackPressed");
		super.onBackPressed();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_delete:
				Intent intent = new Intent();
				intent.putExtra("Id", editId);
				setResult(RESULT_DELETE, intent);
				finish();
				return true;
			case R.id.action_save:
				saveBirthday();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
		// Hide keyboard on date change
		if (view != null) {
			hideKeyboard(view);
		}
	}

	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_ENTER) {
			hideKeyboard(v);
		}
		return false;
	}

	private void hideKeyboard(View v) {
		InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		manager.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	private RadioButton getBoyRadioButton() {
		return (RadioButton) findViewById(R.id.boyRadioButton);
	}

	private RadioButton getGirlRadioButton() {
		return (RadioButton) findViewById(R.id.girlRadioButton);
	}

	private RadioButton getUnspecifiedRadioButton() {
		return (RadioButton) findViewById(R.id.unspecifiedRadioButton);
	}

	private EditText getNameEditText() {
		return (EditText) findViewById(R.id.nameEditText);
	}

	private EditText getNotesEditText() {
		return (EditText) findViewById(R.id.notesEditText);
	}

	private DatePicker getDatePicker() {
		return (DatePicker) findViewById(R.id.birthdatePicker);
	}

	private CheckBox getReminderCheckBox() { return (CheckBox) findViewById(R.id.reminderCheckBox); }
	
}

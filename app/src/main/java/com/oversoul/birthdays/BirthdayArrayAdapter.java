package com.oversoul.birthdays;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oversoul.birthdays.model.Birthday;
import com.oversoul.birthdays.model.BirthdayAgeStyle;

import java.util.ArrayList;


public class BirthdayArrayAdapter extends ArrayAdapter<Birthday> {

	public static final int BG_BIRTHDAY = Color.rgb(0xFF, 0xFD, 0xBB);
	public static final int BG_ALT = Color.rgb(0xFA, 0xFA, 0xF6);

	private final Context context;
	private final ArrayList<Birthday> birthdays;

	public BirthdayArrayAdapter(Context context, ArrayList<Birthday> birthdays) {
		super(context, R.layout.birthday_item, birthdays);
		this.context = context;
		this.birthdays = birthdays;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.birthday_item, parent, false);

		TextView nameView = (TextView) rowView.findViewById(R.id.birthday_name);
		TextView dateView = (TextView) rowView.findViewById(R.id.birthday_date);
		TextView ageView = (TextView) rowView.findViewById(R.id.birthday_age);
		ImageView imgView = (ImageView) rowView.findViewById(R.id.birthday_image);

		Birthday birthday = getItem(position);
		if (birthday.isBoy()) {
			nameView.setTextColor(Color.BLUE);
		} else if (birthday.isGirl()) {
			nameView.setTextColor(Color.MAGENTA);
		} else {
			nameView.setTextColor(Color.DKGRAY);
		}
		nameView.setText(birthday.getName());
		if (birthday.getBirthday() != null) {
			dateView.setText(birthday.getBirthdayString());
			BirthdayAgeStyle age = birthday.getAgeStyle();
			ageView.setText(age.getAge());
			ageView.setTextColor(age.getColor());
			ageView.setTypeface(null, age.getFontStyle());
			if (age.isYearBirthday()) {
				imgView.setVisibility(View.VISIBLE);
				rowView.setBackgroundColor(BG_BIRTHDAY);
			} else {
				imgView.setVisibility(View.GONE);
				if ((position % 2) == 0) {
					rowView.setBackgroundColor(Color.WHITE);
				} else {
					rowView.setBackgroundColor(BG_ALT);
				}
			}
		}

		return rowView;
	}

	public ArrayList<Birthday> getBirthdaysList() {
		return birthdays;
	}

}

package com.oversoul.birthdays.reminder;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.oversoul.birthdays.R;

public class ReminderDialog extends Dialog implements View.OnClickListener {

	private int days = 1;
	private ReminderListener listener;

	public ReminderDialog(Activity activity) {
		super(activity);
	}

	public ReminderDialog(Activity activity, int days, ReminderListener listener) {
		this(activity);
		this.days = days;
		this.listener = listener;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reminder_dialog);

		NumberPicker picker = (NumberPicker) findViewById(R.id.reminderPicker);
		picker.setMinValue(0);
		picker.setMaxValue(7);
		picker.setValue(days);
		picker.setDisplayedValues(new String[] { "Off", "1 day", "2 days", "3 days", "4 days", "5 days", "6 days", "7 days" });

		((Button) findViewById(R.id.okButton)).setOnClickListener(this);
		((Button) findViewById(R.id.cancelButton)).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.okButton) {
			int days = ((NumberPicker) findViewById(R.id.reminderPicker)).getValue();
			setDays(days);
			if (listener != null) {
				listener.reminderSet(this.getDays());
			}
		} else if (v.getId() == R.id.cancelButton) {
			//System.out.println("Cancel");
		}
		dismiss();
	}

}

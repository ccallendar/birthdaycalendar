package com.oversoul.birthdays.service;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;


public class BirthdayReceiver extends WakefulBroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("BirthdayReceiver", "onReceive");
		LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(context);
		lbm.sendBroadcast(new Intent("CHECK_BIRTHDAYS"));
	}
}

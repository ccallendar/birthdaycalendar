package com.oversoul.birthdays.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class BootCompleteReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("BootCompleteReceiver", "onReceive");

		/*Intent birthdayIntent = new Intent(context, BirthdaysListActivity.class);
		birthdayIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(birthdayIntent);*/

		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			if (BirthdayService.getInstance() == null) {
				SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
				boolean notificationsOn = prefs.getBoolean("settingsNotifications", true);
				if (notificationsOn) {
					Intent serviceIntent = new Intent(context, BirthdayService.class);
					Log.i("BootCompleteReciever", "Creating BirthdayService from BootCompleteReciever");
					context.startService(serviceIntent);
				}
			} else {
				Log.i("BootCompleteReciever", "BirthdayService already initialized");
			}
		}
	}

}

package com.oversoul.birthdays.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.oversoul.birthdays.BirthdaysListActivity;
import com.oversoul.birthdays.R;
import com.oversoul.birthdays.db.BirthdayDbHelper;
import com.oversoul.birthdays.model.Birthday;
import com.oversoul.birthdays.model.BirthdayAgeStyle;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Checks each birthday to see if any match today's date.
 */
public class BirthdayService extends JobService {

	private static BirthdayService singleton;

	private static final int BIRTHDAY_NOTIFICATION_ID = 7331;
	private AlarmManager alarmManager;
	private PendingIntent pendingIntent;
	private LocalBirthdayReceiver receiver = new LocalBirthdayReceiver();
	private BirthdayDbHelper dbHelper;

	public BirthdayService() {
		Log.i("BirthdayService", "BirthdayService initialized");
		singleton = this;
    }

	public static BirthdayService getInstance() {
		return singleton;
	}

	@Override
    public void onCreate() {
        super.onCreate();
	    Log.i("BirthdayService", "BirthdayService created");
		dbHelper = new BirthdayDbHelper(this);

		if (isNotificationsOn()) {
		    setupNotifications();
		    scheduleBirthdayCheckNow();
	    } else {
		    stopNotifications();
	    }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("BirthdayService", "BirthdayService destroyed");
	    unregisterReceiver();
    }

	@Override
	public boolean onStartJob(JobParameters params) {
		return false;
	}

	@Override
	public boolean onStopJob(JobParameters params) {
		return false;
	}

	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Messenger callback = intent.getParcelableExtra("messenger");
        Message m = Message.obtain();
        m.what = 2;
        m.obj = this;
        try {
	        if (callback != null) {
		        callback.send(m);
	        }
        } catch (RemoteException e) {
            Log.e("MyService", "Error passing service object back to activity.");
        }
        return START_NOT_STICKY;
    }

	public boolean isNotificationsOn() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
		return prefs.getBoolean("settingsNotifications", true);
	}

	public void setupNotifications() {
		if (alarmManager == null) {
			Log.i("BirthdayService", "Starting notifications");
			alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
			Intent intent = new Intent(this, BirthdayReceiver.class);
			pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
			registerReceiver();
		}
	}

	private void stopNotifications() {
		if (alarmManager != null) {
			Log.i("BirthdayService", "Stopping notifications");
			unregisterReceiver();
			alarmManager.cancel(pendingIntent);
			alarmManager = null;
			pendingIntent = null;
		}
	}

	private void registerReceiver() {
		if (!receiver.isRegistered()) {
			LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
			lbm.registerReceiver(receiver, new IntentFilter("CHECK_BIRTHDAYS"));
			receiver.setRegistered(true);
		}
	}

	private void unregisterReceiver() {
		if (receiver.isRegistered()) {
			LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
			lbm.unregisterReceiver(receiver);
			receiver.setRegistered(false);
		}
	}

	public void scheduleBirthdayCheckNow() {
		Log.i("BirthdaysListActivity", "Running birthday alarm service now");
		alarmManager.set(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis(), pendingIntent);
	}

	private void scheduleNextBirthdayCheck() {
		Calendar calendar = Calendar.getInstance();
		// Run at 9am next day (or today if before 9am)
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		calendar.set(Calendar.HOUR_OF_DAY, 9);
		if (hours >= 9) {
			calendar.add(Calendar.DAY_OF_YEAR, 1);
		}
		// Testing
		//calendar.add(Calendar.MINUTE, 1);
		Log.i("BirthdaysListActivity", "Running birthday alarm service next at " + calendar.getTime());
		alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
	}

	public void checkBirthdays() {
		ArrayList<Birthday> birthdays = dbHelper.getBirthdays();
		if (birthdays != null) {
			ArrayList<Birthday> notifyList = new ArrayList<Birthday>();
			ArrayList<Birthday> reminderList = new ArrayList<Birthday>();
			for (Birthday birthday : birthdays) {
				if (birthday.isYearOrMonthBirthdayToday()) {
					Log.i("BirthdayService", "** BIRTHDAY TODAY! " + birthday.getName() + ", " + birthday.getDateNotified() + ", " + birthday.isNotifiedToday());
					if (!birthday.isNotifiedToday()) {
						notifyList.add(birthday);
					} else {
						System.out.println("Already notified");
					}
				} else {
					int reminderDays = birthday.getReminderDays();
					if (reminderDays > 0) {
						int daysAway = birthday.getDaysAway();
						if (reminderDays == daysAway) {
							Log.i("BirthdayService", "** REMINDER! " + birthday.getName() + " is " + daysAway + " days away");
							if (!birthday.isNotifiedToday()) {
								reminderList.add(birthday);
							}
						} else {
							System.out.println(birthday.getName() + " is " + daysAway + " day(s) from birthdate");
						}
					}
				}
			}
			notifyBirthdays(notifyList, false);
			notifyBirthdays(reminderList, true);
			scheduleNextBirthdayCheck();
		} else {
			Log.e("BirthdayService", "Null birthdays");
		}
	}

	private void notifyBirthdays(ArrayList<Birthday> birthdays, boolean isReminder) {
		int size = birthdays.size();
		if (size == 0) {
			return;
		}
		String title = "", msg = "";
		if (size == 1) {
			Birthday birthday = birthdays.get(0);
			if (isReminder) {
				// Reminder
				int days = birthday.getDaysAway();
				title = birthday.getFirstName() + "'s birthday is in " + BirthdayAgeStyle.plural(days, "day");
				msg = birthday.getAgeString(true, true, true);
			} else {
				title = "It's " + birthday.getFirstName() + "'s birthday!";
				msg = birthday.getAgeString(true, false, false);
			}
		} else if (size > 1) {
			if (isReminder) {
				title = size + " upcoming birthdays";
			} else {
				title = size + " birthdays today";
			}
			String names = "";
			for (Birthday birthday : birthdays) {
				if (names.length() > 0) {
					names += ", ";
				}
				names += birthday.getName();
				if (isReminder) {
					names += " (" + birthday.getBirthdayStringThisYear() + " - " +
							birthday.getAgeString(false, false, true).toLowerCase() + ")";
				}
			}
			msg = names;
		}

		doBirthdayNotification(title, msg);

		LocalDate now = new LocalDate();
		for (Birthday birthday : birthdays) {
			birthday.setDateNotified(now);
			dbHelper.setDateNotified(birthday);
		}
	}

	private void doBirthdayNotification(String title, String msg) {
		Notification.Builder builder = new Notification.Builder(this).setVisibility(Notification.VISIBILITY_PUBLIC);
		builder.setSmallIcon(R.drawable.ic_balloon);
		builder.setContentTitle(title);
		builder.setContentText(msg);
		builder.setAutoCancel(true);
		builder.setColor(BirthdayAgeStyle.COLOR_YEAR_BIRTHDAY);
		builder.setCategory(Notification.CATEGORY_REMINDER);
		builder.setOnlyAlertOnce(true);

		Intent intent = new Intent(this, BirthdaysListActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(notificationPendingIntent);
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(BIRTHDAY_NOTIFICATION_ID, builder.build());
	}

	class LocalBirthdayReceiver extends BroadcastReceiver {
		private boolean registered = false;

		public boolean isRegistered() {
			return registered;
		}
		public void setRegistered(boolean registered) {
			this.registered = registered;
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			//System.out.println("Broadcast onReceive local message: " + intent.getAction());
			checkBirthdays();
		}
	}

}

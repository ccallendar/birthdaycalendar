package com.oversoul.birthdays.util;
import android.app.Activity;
import android.app.Dialog;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;

public class FileChooser {

	private static final String PARENT_DIR = "..";

	private final Activity activity;
	private ListView list;
	private Dialog dialog;
	private File rootPath;
	private File currentPath;

	// filter on file extension
	private String extensionFilter = null;
	private boolean directoryChooser = false;
	private FileSelectedListener fileListener;


	// file selection event handling
	public interface FileSelectedListener {
		void fileSelected(File file);
	}

	public FileChooser(Activity activity) {
		this.activity = activity;
		this.dialog = new Dialog(activity);
		this.list = new ListView(activity);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int which, long id) {
				String fileChosen = (String) list.getItemAtPosition(which);
				File chosenFile = getChosenFile(fileChosen);
				if (chosenFile.isDirectory()) {
					if (isDirectoryChooser()) {
						if (fileListener != null) {
							fileListener.fileSelected(chosenFile);
						}
						dialog.dismiss();
					} else {
						refresh(chosenFile);
					}
				} else {
					if (fileListener != null) {
						fileListener.fileSelected(chosenFile);
					}
					dialog.dismiss();
				}
			}
		});
		dialog.setContentView(list);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		rootPath = Environment.getExternalStorageDirectory();
		Log.i("FileChooser", "Root path: " + rootPath.toString());
		refresh(rootPath);
	}

	public FileChooser(Activity activity, String extensionFilter) {
		this(activity);
		this.setExtensionFilter(extensionFilter);
	}

	public FileChooser(Activity activity, FileSelectedListener fileListener) {
		this(activity);
		this.setFileListener(fileListener);
	}

	public FileChooser(Activity activity, FileSelectedListener fileListener, String extensionFilter) {
		this(activity, fileListener);
		this.setExtensionFilter(extensionFilter);
	}

	public FileChooser(Activity activity, boolean directoryChooser) {
		this(activity);
		this.setDirectoryChooser(directoryChooser);
	}

	public FileChooser(Activity activity, FileSelectedListener fileListener, boolean directoryChooser) {
		this(activity, fileListener);
		this.setDirectoryChooser(directoryChooser);
	}

	public String getExtensionFilter() {
		return extensionFilter;
	}

	public void setExtensionFilter(String extensionFilter) {
		this.extensionFilter = (extensionFilter == null ? null : extensionFilter.toLowerCase());
	}

	public boolean isDirectoryChooser() {
		return directoryChooser;
	}

	public void setDirectoryChooser(boolean directoryChooser) {
		this.directoryChooser = directoryChooser;
	}

	public FileChooser setFileListener(FileSelectedListener fileListener) {
		this.fileListener = fileListener;
		return this;
	}

	public void show() {
		dialog.show();
	}

	/**
	 * Sort, filter and display the files for the given path.
	 */
	private void refresh(File path) {
		this.currentPath = path;
		if (path.exists()) {
			File[] dirs = path.listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					return (file.isDirectory() && file.canRead());
				}
			});
			File[] files = null;
			if (!isDirectoryChooser()) {
				files = path.listFiles(new FileFilter() {
					@Override
					public boolean accept(File file) {
						if (file.isDirectory()) {
							return false;
						}
						if (!file.canRead()) {
							return false;
						} else if ((extensionFilter != null) && (extensionFilter.length() > 0)) {
							return file.getName().toLowerCase().endsWith(extensionFilter);
						}
						return true;
					}
				});
			}
			if (dirs == null) {
				dirs = new File[0];
			}
			if (files == null) {
				files = new File[0];
			}

			int i = 0;
			String[] fileList = null;
			if ((path.getParentFile() != null) && !this.currentPath.equals(this.rootPath)) {
				fileList = new String[dirs.length + files.length + 1];
				fileList[i++] = PARENT_DIR;
			} else {
				fileList = new String[dirs.length + files.length];
			}

			Arrays.sort(dirs);
			Arrays.sort(files);
			for (File dir : dirs) {
				fileList[i++] = dir.getName();
			}
			for (File file : files) {
				fileList[i++] = file.getName();
			}

			// refresh the user interface
			dialog.setTitle(currentPath.getPath());
			list.setAdapter(new ArrayAdapter(activity, android.R.layout.simple_list_item_1, fileList) {
				@Override
				public View getView(int pos, View view, ViewGroup parent) {
					view = super.getView(pos, view, parent);
					((TextView) view).setSingleLine(true);
					return view;
				}
			});
		}
	}

	/**
	 * Convert a relative filename into an actual File object.
	 */
	private File getChosenFile(String fileChosen) {
		if (PARENT_DIR.equals(fileChosen)) {
			return currentPath.getParentFile();
		} else {
			return new File(currentPath, fileChosen);
		}
	}

}
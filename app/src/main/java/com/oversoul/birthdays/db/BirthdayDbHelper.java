package com.oversoul.birthdays.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;

import com.oversoul.birthdays.model.Birthday;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;


/**
 * Created by ccallendar on 15-11-12.
 */
public class BirthdayDbHelper extends SQLiteOpenHelper {

	private static final String PACKAGE_NAME = "com.oversoul.birthdays";
	public static final String DB_NAME = "Birthdays.db";
	public static final int DB_VERSION = 5;
	private static final DateTimeFormatter DB_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");


	public BirthdayDbHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(BirthdayDb.SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.printf("Upgrading db from version %s to %s%n", oldVersion, newVersion);

		boolean upgraded = false;
		if ((oldVersion == 3) && (newVersion >= 4)) {
			db.execSQL(BirthdayDb.SQL_UPGRADE_3_4);
			oldVersion = 4;
			upgraded = true;
		}

		if ((oldVersion == 4) && (newVersion >= 5)) {
			db.execSQL(BirthdayDb.SQL_UPGRADE_4_5);
			upgraded = true;
		}

		if (!upgraded) {
			db.execSQL(BirthdayDb.SQL_DROP_TABLE);
			this.onCreate(db);
		}
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		this.onUpgrade(db, oldVersion, newVersion);
	}

	public ArrayList<Birthday> getBirthdays() {
		ArrayList<Birthday> birthdays = new ArrayList<Birthday>();

		String[] projection = {
				BirthdayDb._ID,
				BirthdayDb.COL_NAME,
				BirthdayDb.COL_BIRTHDAY,
				BirthdayDb.COL_BOY,
				BirthdayDb.COL_NOTES,
				BirthdayDb.COL_DATE_NOTIFIED,
				BirthdayDb.COL_REMINDER
		};
		String sortOrder = BirthdayDb.COL_NAME;

		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.query(BirthdayDb.TABLE, projection, null, null, null, null, sortOrder);
		if (c.moveToFirst()) {
			do {
				int id = c.getInt(c.getColumnIndex(BirthdayDb._ID));
				String name = c.getString(c.getColumnIndex(BirthdayDb.COL_NAME));
				String dateString = c.getString(c.getColumnIndex(BirthdayDb.COL_BIRTHDAY));
				int gender = c.getInt(c.getColumnIndex(BirthdayDb.COL_BOY));
				String notes = c.getString(c.getColumnIndex(BirthdayDb.COL_NOTES));
				String dateNotifiedString = c.getString(c.getColumnIndex(BirthdayDb.COL_DATE_NOTIFIED));
				LocalDate dateNotified = (dateNotifiedString != null ? DB_FORMATTER.parseLocalDate(dateNotifiedString) : null);
				int reminderDays = c.getInt(c.getColumnIndex(BirthdayDb.COL_REMINDER));

				Birthday bday = new Birthday(id, name, DB_FORMATTER.parseLocalDate(dateString), gender, notes, dateNotified, reminderDays);
				birthdays.add(bday);
			} while (c.moveToNext());
		}
		//System.out.println("Returning " + birthdays.size() + " birthdays");

		return birthdays;
	}

	public boolean insertBirthday(Birthday bday) {
		boolean ok = false;
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(BirthdayDb.COL_NAME, bday.getName());
		values.put(BirthdayDb.COL_BIRTHDAY, DB_FORMATTER.print(bday.getBirthday()));
		values.put(BirthdayDb.COL_BOY, bday.getGender());
		values.put(BirthdayDb.COL_NOTES, bday.getNotes());
		values.put(BirthdayDb.COL_REMINDER, bday.getReminderDays());
		int newId = (int) db.insert(BirthdayDb.TABLE, null, values);
		if (newId != -1) {
			bday.setId(newId);
			ok = true;
		}
		return ok;
	}

	public boolean updateBirthday(Birthday bday) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(BirthdayDb.COL_NAME, bday.getName());
		values.put(BirthdayDb.COL_BIRTHDAY, DB_FORMATTER.print(bday.getBirthday()));
		values.put(BirthdayDb.COL_BOY, bday.getGender());
		values.put(BirthdayDb.COL_NOTES, bday.getNotes());
		values.put(BirthdayDb.COL_REMINDER, bday.getReminderDays());
		LocalDate dateNotified = bday.getDateNotified();
		String dateNotifiedString = (dateNotified != null ? DB_FORMATTER.print(dateNotified) : null);
		values.put(BirthdayDb.COL_DATE_NOTIFIED, dateNotifiedString);
		String where = "_id = " + bday.getId();
		int rows = db.update(BirthdayDb.TABLE, values, where, null);
		return (rows == 1);
	}

	public boolean deleteBirthday(int id) {
		SQLiteDatabase db = getWritableDatabase();
		String where = "_id = " + id;
		int rows = db.delete(BirthdayDb.TABLE, where, null);
		return (rows == 1);
	}

	public boolean setDateNotified(Birthday bday) {
		SQLiteDatabase db = getWritableDatabase();
		ContentValues values = new ContentValues();
		LocalDate dateNotified = bday.getDateNotified();
		String dateNotifiedString = (dateNotified != null ? DB_FORMATTER.print(dateNotified) : null);
		values.put(BirthdayDb.COL_DATE_NOTIFIED, dateNotifiedString);
		String where = "_id = " + bday.getId();
		int rows = db.update(BirthdayDb.TABLE, values, where, null);
		return (rows == 1);
	}

	public boolean exportDb(Context context, boolean overwrite) {
		return exportDb(context, overwrite, true);
	}

	private boolean exportDb(Context context, boolean overwrite, boolean makeToast) {
		String date = DB_FORMATTER.print(new LocalDate().toDate().getTime());
		String exportDbPath = "/Temp/" + DB_NAME.replace(".db", "") + "_v" + DB_VERSION + "_" + date + ".db";
		System.out.println("Exporting to " + exportDbPath);
		File exportDbFile = new File(Environment.getExternalStorageDirectory(), exportDbPath);
		return exportDb(exportDbFile, true, context, makeToast);
	}

	public boolean exportDb(File exportDbFile, boolean overwrite, Context context, boolean makeToast) {
		boolean ok = false;
		try {
			File data = Environment.getDataDirectory();

			exportDbFile.getParentFile().mkdirs();
			if (exportDbFile.exists()) {
				if (overwrite) {
					exportDbFile.delete();
				} else {
					System.out.println("Export file already exists: " + exportDbFile.toString());
					return false;
				}
			}
			String currentDbPath = "/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
			File dbFile = new File(data, currentDbPath);
			if (dbFile.exists()) {
				copyFile(dbFile, exportDbFile);
				if (makeToast) {
					Toast.makeText(context, "Saved db to " + exportDbFile.toString(), Toast.LENGTH_LONG).show();
				}
				ok = true;
			} else {
				throw new FileNotFoundException("Database file doesn't exist - " + dbFile.toString());
			}
		} catch (Exception e) {
			System.err.println("Error exporting database: " + e.getMessage());
			if (makeToast) {
				Toast.makeText(context, "Error exporting database.\n" + e.toString(), Toast.LENGTH_LONG).show();
			}
		}
		return ok;
	}

	/**
	 * Imports birthdays from another database file.
	 * All existing birthdays are deleted.
	 * If you want to preserve them then call getBirthdays() first, and then addBirthdays() after importing.
	 * @param importDbFile
	 * @param context
	 * @return boolean true if successful
	 */
	public boolean importDb(File importDbFile, Context context) {
		boolean ok = false;

		// Make a backup
		ArrayList<Birthday> birthdays = getBirthdays();
		if (birthdays.size() > 0) {
			exportDb(context, false, false);
		}

		try {
			close();
			File data  = Environment.getDataDirectory();
			String currentDbPath = "/data/" + PACKAGE_NAME + "/databases/" + DB_NAME;
			copyFile(importDbFile, new File(data, currentDbPath));
			getWritableDatabase().close();
			Toast.makeText(context, "Imported db from: " + importDbFile.toString(), Toast.LENGTH_LONG).show();
			ok = true;
		} catch (Exception e) {
			Toast.makeText(context, "Error importing: " + e.toString(), Toast.LENGTH_LONG).show();
		}

		return ok;
	}

	private void copyFile(File srcFile, File destFile) throws IOException {
		FileChannel src = new FileInputStream(srcFile).getChannel();
		FileChannel dst = new FileOutputStream(destFile).getChannel();
		dst.transferFrom(src, 0, src.size());
		src.close();
		dst.close();
	}

	/**
	 * Adds new birthdays - duplicates are not added.
	 */
	public int addBirthdays(ArrayList<Birthday> birthdays) {
		int added = 0;
		ArrayList<Birthday> currentBirthdays = getBirthdays();
		for (Birthday bday : birthdays) {
			boolean found = false;
			for (Birthday current : currentBirthdays) {
				if (current.isEqual(bday)) {
					found = true;
					break;
				}
			}
			if (!found) {
				System.out.println("Adding birthday: " + bday.toString());
				insertBirthday(bday);
				added++;
			} else {
				System.out.println("Birthday already exists");
			}
		}
		return added;
	}

}

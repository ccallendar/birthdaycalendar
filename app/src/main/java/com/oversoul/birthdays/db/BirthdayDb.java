package com.oversoul.birthdays.db;

import android.provider.BaseColumns;

public class BirthdayDb implements BaseColumns {

	public static final String TABLE = "birthdays";
	public static final String COL_NAME = "name";
	public static final String COL_BIRTHDAY = "birthday";
	public static final String COL_BOY = "boy";
	public static final String COL_NOTES = "notes";
	public static final String COL_DATE_NOTIFIED = "notified";
	public static final String COL_REMINDER = "reminder";

	public static final String SQL_DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE;

	public static final String SQL_CREATE_TABLE = String.format("CREATE TABLE %s " +
			"(%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER)",
			TABLE, _ID, COL_NAME, COL_BIRTHDAY, COL_BOY, COL_NOTES, COL_DATE_NOTIFIED, COL_REMINDER);

	public static final String SQL_UPGRADE_3_4 = String.format("ALTER TABLE %s ADD COLUMN %s TEXT", TABLE, COL_DATE_NOTIFIED);
	public static final String SQL_UPGRADE_4_5 = String.format("ALTER TABLE %s ADD COLUMN %s INTEGER", TABLE, COL_REMINDER);


}
